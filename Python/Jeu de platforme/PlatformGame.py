import datetime
import pickle
import pygame
from pygame.locals import *
from pygame import mixer
from os import path
from classes.Enemy import *
from classes.Lava import *
from classes.Energy import *
from classes.Exit import *
from classes.Bouton import *
from classes.Canon_garic import *
from classes.Platform import *
from classes.Key import *
from classes.Bullet import *
from classes.Definission import *

#create frame
pygame.mixer.pre_init(44100, -16, 2, 512)
pygame.display.set_caption('PlatformGame')
pygame_icon = pygame.image.load('favicon.ico')
pygame.display.set_icon(pygame_icon)
mixer.init()
pygame.init()

clock = pygame.time.Clock()

font_score = pygame.font.SysFont('Bauhaus 93', 30)
font_time = pygame.font.SysFont('Bauhaus 93', 30)
font = pygame.font.SysFont('Bauhaus 93', 70)

# game variables
caractere = "" #caractere name
game_over = 0 
dialogue = False #display help
main_menu = True #display menu
select_caractere = False #display caractere select
init_caractere = False
level = 0 # level id
max_levels = 0
change_charactere = False
alpha = 200 #opacity
tmp_bullet = 90

white = (255,255,255)
blue = (0,0,255)

def reset_sprites():
    blob_group.empty()
    bug_group.empty()
    shooter_group.empty()
    devil_group.empty()
    lava_group.empty()
    platform_group.empty()
    exit_group.empty()
    key_group.empty()
    energy_group.empty()
    bullet_group.empty()

def reset_level(level, deth_num, energy_count, key):
    if level == 0:
        player.reset(40, screen_height - 130, deth_num,energy_count, key)
    else:
        player.next_level(40, screen_height - 130, deth_num,energy_count, player.transform)
    reset_sprites()
    if path.exists(f'level/level{level}_data'):
        pickle_in = open(f'level/level{level}_data', 'rb')
        world_data = pickle.load(pickle_in)
    world = World(world_data)

    return world     

################################################################################################
######################################### CLASS PLAYER #########################################
################################################################################################
class Player():
    def __init__(self, x, y, deth_num, energy_count):
        self.reset(x, y, deth_num, energy_count, False)

    def enemy_colision(self, group, game_over):
        if pygame.sprite.spritecollide(self, group, False):
            if self.transform == True:
                self.transform = False
                hit_fx.play()
                if self.direction == 0:
                    self.rect.x -= 30
                else :
                    self.rect.x += 30
            else:
                game_over = -1
                self.death_num += 1
                game_over_fx.play()
        return game_over

    def attack_colision(self, group):
        for enemy in pygame.sprite.spritecollide(canon_garic, group, False):
                hit_fx.play()
                enemy.kill()

    def update(self, game_over, tmp_bullet):
        dx = 0
        dy = 0
        walk_counter = 5
        col_thresh = 20

        if game_over == 0:
            key = pygame.key.get_pressed()
            # super saiyen
            if key[pygame.K_t]:
                if self.transform == False  and self.energy >= 2:
                    self.counter += 1
                    if self.counter > walk_counter:
                        self.counter = 0
                        self.index += 1
                        if self.index >= len(self.images_transform):
                            self.index = 0
                            self.transform = True
                            self.energy -= 2
                        else:
                            if self.index == 1:
                                if caractere == "vegeta" or caractere == "naruto" :
                                    saiyan_aura_fx.play()
                                elif caractere == "samus":
                                    samus1_fx.play()
                                elif caractere == "mario":
                                    mario_fx.play()
                            self.image = self.images_transform[self.index]
            # normal state
            if key[pygame.K_y]:
                if self.transform == True:
                    self.counter += 1
                    if self.counter > walk_counter:
                        self.counter = 0
                        self.index += 1
                        if self.index >= len(self.images_anti_transform):
                            self.index = 0
                            self.transform = False
                        else:
                            if self.index == 1:
                                if caractere == "vegeta" or caractere == "naruto" :
                                    saiyan_off_fx.play()
                                elif caractere == "samus":
                                    samus2_fx.play()
                                elif caractere == "mario":
                                    mario_fx.play()
                            self.image = self.images_anti_transform[self.index]
            # canon garic
            if key[pygame.K_c]:
                if self.canon == False and self.energy >= 1:
                    self.counter += 1
                    if self.counter > walk_counter:
                        self.counter = 0
                        self.index += 1
                        if self.index >= len(self.images_canon):
                            self.index = 0
                        else:
                            if self.index == 1:
                                if caractere == "vegeta":
                                    galic_gun_fx.play()
                                elif caractere == "naruto":
                                    rasengan_fx.play()
                            if self.index == 3:
                                #final flash
                                galic_gun_blast_fx.play()
                                self.canon = True
                                self.energy -= 1
                                canon_garic.attack(self.rect.x, self.rect.y, self.direction, player.rect.width)
                            if self.direction == 1:
                                if self.transform == False:
                                    self.image = self.images_canon[self.index]
                                else:
                                    self.image = self.images_transform_canon[self.index]
                            else:
                                if self.transform == False:
                                    self.image = self.images_canon_left[self.index]
                                else:
                                    self.image = self.images_transform_canon_left[self.index]    
            # deplacements
            if key[pygame.K_LEFT]:
                dx -= 5
                self.counter += 1
                self.direction = -1
            if key[pygame.K_RIGHT]:
                dx += 5
                self.counter += 1
                self.direction = 1
            if (key[pygame.K_SPACE] or key[pygame.K_UP]) and self.jumped == False and self.in_air == False:
                jump_fx.play()
                self.vel_y = -15
                self.jumped = True
            if key[pygame.K_SPACE] == False:
                self.jumped = False
            if key[pygame.K_LEFT] == False and key[pygame.K_RIGHT] == False and key[pygame.K_t] == False and key[pygame.K_y] == False and key[pygame.K_c] == False:
                self.counter = 0
                self.index = 0
                self.canon = False
                canon_garic.clear()
                if self.transform == False:
                    if self.direction == 1:
                        self.image = self.images_right[self.index]
                    if self.direction == -1:
                        self.image = self.images_left[self.index]
                else:
                    if self.direction == 1:
                        self.image = self.images_right_transform[self.index]
                    if self.direction == -1:
                        self.image = self.images_left_transform[self.index]

            #animation
            if self.counter > walk_counter:
                self.counter = 0
                self.index = 1
                if self.transform == False:
                    if self.direction == 1:
                        self.image = self.images_right[self.index]
                    if self.direction == -1:
                        self.image = self.images_left[self.index]
                else:
                    if self.direction == 1:
                        self.image = self.images_right_transform[self.index]
                    if self.direction == -1:
                        self.image = self.images_left_transform[self.index]

            # gravité
            if self.canon == False:
                self.vel_y += 1
                if self.vel_y > 10:
                    self.vel_y = 10      
                dy = self.vel_y

            #check collision block
            self.in_air = True
            for tile in world.tile_list:
                # x direction
                if tile[1].colliderect(self.rect.x + dx, self.rect.y, self.width, self.heignt):
                    dx = 0
                # y direction
                if tile[1].colliderect(self.rect.x, self.rect.y + dy, self.width, self.heignt):
                    #check for ground to jump
                    if self.vel_y < 0:
                        dy = tile[1].bottom - self.rect.top
                        self.vel_y = 0
                    elif self.vel_y >= 0:
                        dy = tile[1].top - self.rect.bottom
                        self.vel_y = 0
                        self.in_air = False

            #colision
            game_over = self.enemy_colision(blob_group, game_over)
            game_over = self.enemy_colision(bug_group, game_over)
            game_over = self.enemy_colision(shooter_group, game_over)
            game_over = self.enemy_colision(lava_group, game_over)
            game_over = self.enemy_colision(bullet_group, game_over)
            game_over = self.enemy_colision(devil_group, game_over)

            #check collision attack
            self.attack_colision(blob_group)
            self.attack_colision(bug_group)
            self.attack_colision(shooter_group)
            self.attack_colision(devil_group)


            #check for colision with moving block
            for platform in platform_group:
                # x direction
                if platform.rect.colliderect(self.rect.x + dx, self.rect.y, self.width, self.heignt):
                    dx = 0
                if platform.rect.colliderect(self.rect.x, self.rect.y + dy, self.width, self.heignt):
                    # caractere under
                    if abs((self.rect.top + dy) - platform.rect.bottom) < col_thresh:
                        self.vel_y = 0
                        dy = platform.rect.bottom - self.rect.top
                    # caractere on
                    elif abs((self.rect.bottom + dy) - platform.rect.top) < col_thresh:
                        self.rect.bottom = platform.rect.top - 1
                        dy = 0    
                        self.in_air = False
                    #move with platform
                    if platform.move_x != 0:
                        self.rect.x += platform.move_direction

            #check exit
            for exit in pygame.sprite.spritecollide(self, exit_group, False):
                if exit.id == 0 or exit.id == 1 and self.key == True:   
                    game_over = 1

            #check key
            for key in pygame.sprite.spritecollide(self, key_group, False):
                self.key = True
                key_group.remove(key)
                energy_fx.play()

            # update player position
            self.rect.x += dx
            self.rect.y += dy

        elif game_over == -1:
            self.image = self.dead_image
            draw_text('GAME OVER !', font, blue, (screen_width //2) -150, screen_height //2)
            self.rect.y -= 5

        screen.blit(self.image, self.rect)
        # hit box
        #pygame.draw.rect(screen, (255, 255, 255), self.rect, 2)
        draw_text(('Death: '+ str(self.death_num)), font_time, white, (screen_width //8)*4 , 10)

        return game_over

    def bullet_shoot(self, tmp_bullet):
        #create bullet
        for shooter in shooter_group:
            if shooter.rect.y-10 < self.rect.y + self.heignt and shooter.rect.y + 65 > self.rect.y:
                tmp_bullet += 1
                if tmp_bullet == 100:
                    tmp_bullet = 0
                    if shooter.direction == 1:
                        bullet = Bullet(shooter.rect.x + 40, shooter.rect.y + 12, 1)
                        bullet_group.add(bullet)
                    else:
                        bullet = Bullet(shooter.rect.x - 20, shooter.rect.y + 12, 0)
                        bullet_group.add(bullet)
        return tmp_bullet

    def next_level(self, x, y, death_num, energy_count, transform):
        self.rect.x = x
        self.rect.y = y
        self.vel_y = 0
        self.jumped = False
        self.direction = 0
        self.transform = transform
        self.in_air = True
        self.canon = False
        self.death_num = death_num
        self.energy = energy_count # count energys
        self.key = False


    def reset(self, x, y, death_num, energy_count, key):
        self.images_right = []
        self.images_right_transform = []
        self.images_left = []
        self.images_left_transform = []
        self.images_transform = []
        self.images_anti_transform = []
        self.images_canon = []
        self.images_canon_left = []
        self.images_transform_canon = []
        self.images_transform_canon_left = []
        self.index = 0
        self.counter = 0
        for num in range(1, 3):
            img_right = pygame.image.load('img/' + caractere + '/' + caractere + f'{num}.png')
            img_right_transform = pygame.image.load('img/' + caractere + '/' + caractere + f'_transformation{num}.png')
            img_left = pygame.transform.flip(img_right, True, False)
            img_left_transform = pygame.transform.flip(img_right_transform, True, False)
            img_anti_transform = pygame.image.load('img/' + caractere + '/' + caractere + f'_anti_transformation_{num}.png')
            self.images_anti_transform.append(img_anti_transform)
            self.images_right.append(img_right)
            self.images_right_transform.append(img_right_transform)
            self.images_left.append(img_left)
            self.images_left_transform.append(img_left_transform)
        for num in range(1, 7):
            img_transform = pygame.image.load('img/' + caractere + '/' + caractere + f'_transformation_{num}.png')
            self.images_transform.append(img_transform)
        for num in range(0, 4):
            img_canon = pygame.image.load('img/' + caractere + '/' + caractere + f'_canon{num}.png')
            img_canon_left = pygame.transform.flip(img_canon, True, False)
            img_transform_canon = pygame.image.load('img/' + caractere + '/' + caractere + f'_transform_canon{num}.png')
            img_transform_canon_left = pygame.transform.flip(img_transform_canon, True, False)
            self.images_canon.append(img_canon)
            self.images_canon_left.append(img_canon_left)
            self.images_transform_canon.append(img_transform_canon)
            self.images_transform_canon_left.append(img_transform_canon_left)
        self.dead_image = pygame.image.load(f'img/ghost.png')
        self.dead_image.fill((255, 255, 255, alpha), None, pygame.BLEND_RGBA_MULT)
        self.image = self.images_right[self.index]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.width = self.image.get_width()
        self.heignt = self.image.get_height()
        self.vel_y = 0
        self.jumped = False
        self.direction = 0
        self.transform = False
        self.in_air = True
        self.canon = False
        self.death_num = death_num
        self.energy = energy_count # count energys
        self.key = key

###############################################################################################
######################################### CLASS WORLD #########################################
###############################################################################################
class World():
    def __init__(self, data):
        self.tile_list = []

        dirt_img = pygame.image.load('img/dirt.png')
        grass_img = pygame.image.load('img/grass.png')

        row_count = 0
        for row in data:
            col_count = 0
            for tile in row:
            # bloc
                if tile == 1:
                    img = pygame.transform.scale(dirt_img, (tile_size, tile_size))
                    img_rect = img.get_rect()
                    img_rect.x = col_count * tile_size
                    img_rect.y = row_count * tile_size
                    tile = (img, img_rect)
                    self.tile_list.append(tile)
                if tile == 2:
                    img = pygame.transform.scale(grass_img, (tile_size, tile_size))
                    img_rect = img.get_rect()
                    img_rect.x = col_count * tile_size
                    img_rect.y = row_count * tile_size
                    tile = (img, img_rect)
                    self.tile_list.append(tile)
                if tile == 3:
                    platform = Platform(col_count * tile_size, row_count*tile_size, 1, 0)
                    platform_group.add(platform)
                if tile == 4:
                    platform = Platform(col_count * tile_size, row_count*tile_size, 0, 1)
                    platform_group.add(platform)
            # usefull    
                if tile == 5:
                    energy = Energy(col_count * tile_size + (tile_size // 2), row_count*tile_size + tile_size // 2 )
                    energy_group.add(energy)
                if tile == 6:
                    exit = Exit(col_count * tile_size, row_count*tile_size - tile_size // 2 , 0)
                    exit_group.add(exit)
                if tile == 11:
                    exit = Exit(col_count * tile_size, row_count*tile_size - tile_size // 2 , 1)
                    exit_group.add(exit)
                if tile == 12:
                    key = Key(col_count * tile_size, row_count*tile_size - tile_size // 2 )
                    key_group.add(key)
            # enemy
                if tile == 7:
                    lava = Lava(col_count * tile_size, row_count*tile_size + tile_size // 2 )
                    lava_group.add(lava)
                if tile == 8:
                    blob = Enemy(col_count * tile_size, row_count*tile_size, 3)
                    blob_group.add(blob)
                if tile == 9:
                    bug = Enemy(col_count * tile_size, row_count*tile_size, 9)
                    bug_group.add(bug)
                if tile == 10:
                    shooter = Enemy(col_count * tile_size, row_count*tile_size, 10)
                    shooter_group.add(shooter)
                if tile == 13:
                    devil = Enemy(col_count * tile_size, row_count*tile_size, 13)
                    devil_group.add(devil)
                col_count += 1
            row_count += 1

    def draw(self):
        for tile in self.tile_list:
            screen.blit(tile[0], tile[1])

########################################################################################
######################################### MAIN #########################################
########################################################################################    

lava_group = pygame.sprite.Group()
platform_group = pygame.sprite.Group()
blob_group = pygame.sprite.Group()
bug_group = pygame.sprite.Group()
shooter_group = pygame.sprite.Group()
devil_group = pygame.sprite.Group()
exit_group = pygame.sprite.Group()
key_group = pygame.sprite.Group()
energy_group = pygame.sprite.Group()
bullet_group = pygame.sprite.Group()
canon_garic_group = pygame.sprite.Group()

if path.exists(f'level/level{level}_data'):
    pickle_in = open(f'level/level{level}_data', 'rb')
    world_data = pickle.load(pickle_in)
world = World(world_data)

restart_game_over_button = Button(screen_width // 2 - 50, screen_height // 2 + 100, restart_game_over_img)
start_button = Button(screen_width // 2 - 350, screen_height // 2, start_img)
exit_button = Button(screen_width // 2 + 100, screen_height // 2, exit_img)
vegeta_button = Button(screen_width // 2 + 100, screen_height // 2 - 150, vegeta_img)
samus_button = Button(screen_width // 2 -193, screen_height // 2 -50, samus_img)
naruto_button = Button(screen_width // 2 -200, screen_height // 2 -150, naruto_img)
mario_button = Button(screen_width // 2 + 100, screen_height // 2 -50, mario_img)
kirby_button = Button(screen_width // 2 -193, screen_height // 2 + 50, kirby_img)
help_img = pygame.transform.scale(help_img, (30, 30))
help_button = Button(screen_width - 35*2, 5, help_img)
chose_caractere_img = pygame.transform.scale(chose_caractere_img, (30, 30))
chose_caractere_button = Button(screen_width - 55*2, 5, chose_caractere_img)
restart_img = pygame.transform.scale(restart_img, (30, 30))
restart_button = Button(screen_width - 75*2, 5, restart_img)
dialogue_img = pygame.transform.scale(dialogue_img, (180*2, 150*2))
dialogue_button = Button(screen_width // 2 - tile_size*4, screen_height // 2 - tile_size*8, dialogue_img)
score_energy_img= pygame.transform.scale(score_energy_img, (tile_size//2, tile_size//2))
key_img= pygame.transform.scale(key_img, (tile_size*1.2, tile_size*0.5))

max_levels = count_maxlevels()

run = True
while run:
    clock.tick(fps)
    screen.blit(bg_img, (0,0))
    key = pygame.key.get_pressed()

    #menu 
    if main_menu == True:
        if exit_button.draw():
            run = False
        if start_button.draw() or key[pygame.K_SPACE]:
            main_menu = False
            select_caractere = True
    #selection personnage
    elif select_caractere == True :
        draw_text('Select your charactere', font, blue, screen_width //2 - 250, screen_height //2 - 200)
        if vegeta_button.draw():
            caractere = "vegeta"
            select_caractere = False
            init_caractere = True
        if naruto_button.draw():
            caractere = "naruto"
            select_caractere = False
            init_caractere = True
        if samus_button.draw():
            caractere = "samus"
            select_caractere = False
            init_caractere = True
        if mario_button.draw():
            caractere = "mario"
            select_caractere = False
            init_caractere = True
        if kirby_button.draw():
            caractere = "kirby"
            select_caractere = False
            init_caractere = True
    #initialisation
    elif init_caractere == True:
        player = Player(40, screen_height - 130, 0, 0)
        init_caractere = False
        canon_garic = Canon_garic(player.rect.x, player.rect.y)
        canon_garic_group.add(canon_garic)
        if not change_charactere:
            start_time = datetime.datetime.now()
    else:
        screen.blit(sun_img,(100*(level+1),100))
        world.draw()
        screen.blit(score_energy_img,(7, 7))
        if player.key:
            screen.blit(key_img,(tile_size*2, 7))

        #partie en cours
        if game_over == 0:
            date_now = datetime.datetime.now() - start_time    
            blob_group.update(player.rect.x)
            bug_group.update(player.rect.x)
            shooter_group.update(player.rect.x)
            bullet_group.update()
            devil_group.update(player.rect.x)
            lava_group.update()
            platform_group.update()
            if pygame.sprite.spritecollide(player, energy_group, True):
                energy_fx.play()
                player.energy += 1
            
        draw_text('x '+ str(player.energy), font_score, white, tile_size -10, 10)
        draw_text(('Time: '+ str(date_now)), font_time, white, screen_width //8 , 10)
        draw_text(('Level: '+ str(level)), font_time, white, (screen_width //8)*5 , 10)

        blob_group.draw(screen)
        bug_group.draw(screen)
        shooter_group.draw(screen)
        devil_group.draw(screen)
        lava_group.draw(screen)
        platform_group.draw(screen)
        exit_group.draw(screen)
        key_group.draw(screen)
        energy_group.draw(screen)
        bullet_group.draw(screen)
        canon_garic_group.draw(screen)

        game_over = player.update(game_over,tmp_bullet)
        tmp_bullet = player.bullet_shoot(tmp_bullet)

        # partie perdu
        if game_over == -1:
            if restart_game_over_button.draw() or key[pygame.K_SPACE]:
                player.reset(40, screen_height - 130, player.death_num, player.energy, player.key)
                canon_garic.clear()
                game_over = 0
                player.energy = 0
        #partie gagnée
        if game_over == 1:
            if level <= max_levels:
                level += 1
            if level <= max_levels:
                world_data = []
                world = reset_level(level, player.death_num, player.energy, False)
                game_over = 0
            else:
                draw_text('YOU WIN !', font, blue, screen_width //2 -120 , screen_height //2)
                if restart_game_over_button.draw():
                    start_time = datetime.datetime.now()
                    level = 0
                    world_data = []
                    world = reset_level(level, 0, 0, False)
                    game_over = 0
                    player.energy = 0
        
        if game_over == 0:
            #choose caractere button
            if chose_caractere_button.draw():
                select_caractere = True
                change_charactere = True

            #restart button
            if restart_button.draw():
                world = reset_level(0, 0, 0, False)
                start_time = datetime.datetime.now()
                level = 0
    
    #help button
    if help_button.draw() or dialogue == True:
        dialogue = True
        if dialogue_button.draw(): 
            dialogue = False

    #draw_grid()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    pygame.display.update()

pygame.quit()