from classes.Definission import *

class Canon_garic(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        img = pygame.image.load('img/empty.png')
        self.image = pygame.transform.scale(img, (90, 70))
        self.rect = self.image.get_rect()
        self.rect.x = x -8
        self.rect.y = y +1

    def attack(self, x, y, direction , width):
        if direction == 1:
            self.rect.x = x + width
            self.rect.y = y -9
            self.image = pygame.image.load('img/vegeta_canon.png')
        else:
            self.rect.x = x -90
            self.rect.y = y -9
            img = pygame.image.load('img/vegeta_canon.png')
            self.image = pygame.transform.flip(img, True, False)
            
    def clear(self):
        self.rect.x = -272
        self.rect.y = screen_height
        self.image = pygame.image.load('img/empty.png')
