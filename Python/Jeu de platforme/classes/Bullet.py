from classes.Definission import *

class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y, direction):
        pygame.sprite.Sprite.__init__(self)
        img = pygame.image.load('img/bullet.png')
        self.image = pygame.transform.scale(img, (tile_size/2, tile_size /5))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.direction = direction

    def update(self):
        if self.direction == 1:
            self.rect.x += 3
        else:
            self.rect.x -= 3