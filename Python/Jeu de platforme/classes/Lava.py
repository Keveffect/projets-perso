from classes.Definission import *

class Lava(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        img = pygame.image.load('img/lava.png')
        self.image = pygame.transform.scale(img, (tile_size, tile_size // 2))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.time = 0

    def update(self):
        self.time += 1
        if self.time > 50:
            self.time = 0
            self.image = pygame.transform.flip(self.image, True, False)