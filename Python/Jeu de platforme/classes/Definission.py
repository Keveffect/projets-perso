import pickle
import tkinter
import pygame
from pygame.locals import *
from pygame import mixer
from os import path

clock = pygame.time.Clock()
fps = 60

#size of the screen
taille_ecran = tkinter.Tk().winfo_screenheight()
if taille_ecran >= 800:
    taux_ecran = 1
else:
    taux_ecran = 0.875
screen_width = 1200 * taux_ecran
screen_height = 800* taux_ecran

screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Pygame')

# game variables
tile_size = 40 * taux_ecran # size scare gride
game_over = 0
main_menu = True
level = 0
max_levels = 6
score = 0

white = (255,255,255)
blue = (0,0,255)

#load images
sun_img = pygame.image.load('img/sun.png')
score_energy_img = pygame.image.load('img/energy.png')
bg_img = pygame.image.load('img/background1.png')
key_img = pygame.image.load('img/key.png')
restart_game_over_img = pygame.image.load('img/restart_btn.png')
start_img = pygame.image.load('img/start_btn.png')
help_img = pygame.image.load('img/help.png')
restart_img = pygame.image.load('img/restart.png')
chose_caractere_img = pygame.image.load('img/chose_caractere.png')
dialogue_img = pygame.image.load('img/dialogue.png')
vegeta_img = pygame.image.load('img/vegeta/vegeta_transformation_6.png')
samus_img = pygame.image.load('img/samus/samus_transformation_2.png')
naruto_img = pygame.image.load('img/naruto/naruto_transformation_6.png')
mario_img = pygame.image.load('img/mario/mario_transformation_6.png')
kirby_img = pygame.image.load('img/kirby/kirby_transformation_6.png')
exit_img = pygame.image.load('img/exit_btn.png')
empty_img = pygame.image.load('img/empty.png')
#canon garic
canon_img = pygame.image.load('img/vegeta_canon.png')

#load soudns
pygame.mixer.pre_init(44100, -16, 2, 512)
mixer.init()

#load soudns
pygame.mixer.music.load('sound/music.wav')
#pygame.mixer.music.play(-1, 0.0, 5000)
galic_gun_fx = pygame.mixer.Sound('sound/galic_gun.wav')
galic_gun_fx.set_volume(0.4)
rasengan_fx = pygame.mixer.Sound('sound/rasengan.wav')
rasengan_fx.set_volume(0.5)
galic_gun_blast_fx = pygame.mixer.Sound('sound/galic_gun_blast.wav')
galic_gun_blast_fx.set_volume(0.3)
saiyan_aura_fx = pygame.mixer.Sound('sound/saiyan_aura.wav')
saiyan_aura_fx.set_volume(0.5)
saiyan_off_fx = pygame.mixer.Sound('sound/saiyan_off.wav')
saiyan_off_fx.set_volume(0.5)
energy_fx = pygame.mixer.Sound('sound/coin.wav')
energy_fx.set_volume(0.5)
jump_fx = pygame.mixer.Sound('sound/jump.wav')
jump_fx.set_volume(0.5)
game_over_fx = pygame.mixer.Sound('sound/game_over.wav')
game_over_fx.set_volume(0.5)
hit_fx = pygame.mixer.Sound('sound/hit.wav')
hit_fx.set_volume(0.5)
samus1_fx = pygame.mixer.Sound('sound/samus1.mp3')
samus1_fx.set_volume(0.5)
samus2_fx = pygame.mixer.Sound('sound/samus2.mp3')
samus2_fx.set_volume(0.5)
mario_fx = pygame.mixer.Sound('sound/mario.mp3')
mario_fx.set_volume(0.5)

def count_maxlevels():
    count_level = 0
    while path.exists(f'level/level{count_level}_data'):
            count_level += 1
    return count_level - 1

def draw_text(text, font, text_col, x, y):
    img = font.render(text, True, text_col)
    screen.blit(img, (x,y))

def draw_grid():
    for line in range(0, 20):
        pygame.draw.line(screen, (255,255,255), (0, line * tile_size), (screen_width, line* tile_size))
        pygame.draw.line(screen, (255,255,255), (line * tile_size, 0), (line* tile_size, screen_height))