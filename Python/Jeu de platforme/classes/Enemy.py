from pickle import TRUE
from shutil import move
from classes.Definission import *
import random

class Enemy(pygame.sprite.Sprite):
    def __init__(self, x, y, num):
        pygame.sprite.Sprite.__init__(self)
        if num == 3:
            rand = abs(random.randrange(4))
            self.image = pygame.image.load('img/blob'+str(rand)+'.png')
        if num == 9:
            self.image = pygame.image.load('img/bug.png')
            self.image = pygame.transform.scale(self.image, (tile_size, tile_size))
        if num == 10:
            self.image = pygame.image.load('img/shooter.png')
            self.image = pygame.transform.scale(self.image, (tile_size, tile_size))
        if num == 13:
            self.image = pygame.image.load('img/devil/devil1.png')
            self.image = pygame.transform.scale(self.image, (40*5, 60*5))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.num = num
        self.move_direction = 1
        self.move_counter = 0
        self.player_seen = False
        self.direction = 0 #gauche

    def update(self, playerX):
        #blob
        if self.num == 3:
            self.rect.x += self.move_direction
            self.move_counter += 1
            if abs(self.move_counter) > 50:
                self.move_direction *= -1
                self.move_counter *= -1

        #bug
        if self.num == 9:
            if self.rect.x-40 < playerX:
                self.player_seen = True
            if self.num == 9 and self.rect.y < 800 and self.player_seen == True:
                self.rect.y += self.move_direction

        #shooter
        if self.num == 10:
            if self.direction == 0 and self.rect.x-10 < playerX:
                self.image = pygame.transform.flip(self.image, True, False)
                self.direction = 1
            elif self.direction == 1 and self.rect.x-10 > playerX:
                self.image = pygame.transform.flip(self.image, True, False)
                self.direction = 0
    
        #devil
        if self.num == 13:
            self.move_counter += 1
            if self.move_counter <= 20:
                self.image = self.image = pygame.image.load('img/devil/devil1.png')
            if self.move_counter > 20 and self.move_counter <= 40:
                self.image = self.image = pygame.image.load('img/devil/devil2.png')
            if self.move_counter > 40 and self.move_counter <= 60:
                self.image = self.image = pygame.image.load('img/devil/devil3.png')
            if self.move_counter > 60 and self.move_counter <= 80:
                if self.move_counter == 80:
                    self.move_counter = 1
                self.image = self.image = pygame.image.load('img/devil/devil2.png')
            self.image = pygame.transform.scale(self.image, (40*5, 60*5))