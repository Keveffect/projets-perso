from classes.Definission import *

class Exit(pygame.sprite.Sprite):
    def __init__(self, x, y, id):
        pygame.sprite.Sprite.__init__(self)
        self.id = id
        if self.id == 0:
            img = pygame.image.load('img/exit.png')
        elif self.id == 1:
            img = pygame.image.load('img/exit_lock.png')
        self.image = pygame.transform.scale(img, (tile_size, int(tile_size * 1.5)))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y 