import turtle
import time

gameOver = False

wn = turtle.Screen()
wn.title("Pong")
wn.bgcolor("black")
wn.setup(width=800, height=600)
wn.tracer(0)

# score
score_a = 0
score_b = 0

# paddle A
paddle_a = turtle.Turtle()
paddle_a.speed(0)
paddle_a.shape("square")
paddle_a.color("blue")
paddle_a.shapesize(stretch_wid=5, stretch_len=1)
paddle_a.penup()
paddle_a.goto(-350, 0)

# paddle B
paddle_b = turtle.Turtle()
paddle_b.speed(0)
paddle_b.shape("square")
paddle_b.color("red")
paddle_b.shapesize(stretch_wid=5, stretch_len=1)
paddle_b.penup()
paddle_b.goto(350, 0)

#ball
ball = turtle.Turtle()
ball.speed(0)
ball.shape("square")
ball.color("white")
ball.shapesize(stretch_wid=1, stretch_len=1)
ball.penup()
ball.goto(0, 0)
ball.dx = 0.2
ball.dy = -0.2

# pen
pen = turtle.Turtle()
pen.speed(0)
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 256)
pen.write("Joueur Bleu: {}  Joueur Rouge: {}".format(score_a, score_b), align="center", font=("Courier", 24, "normal"))

penGagnantBleu = turtle.Turtle()
penGagnantBleu.speed(0)
penGagnantBleu.color("blue")
penGagnantBleu.penup()
penGagnantBleu.hideturtle()
penGagnantBleu.goto(0, 0)

penGagnantRed = turtle.Turtle()
penGagnantRed.speed(0)
penGagnantRed.color("red")
penGagnantRed.penup()
penGagnantRed.hideturtle()
penGagnantRed.goto(0, 0)

# fonction
def paddle_a_up():
    if paddle_a.ycor() < 250:
        y = paddle_a.ycor()
        y += 20
        paddle_a.sety(y)

def paddle_b_up():
    if paddle_b.ycor() < 250:
        y = paddle_b.ycor()
        y += 20
        paddle_b.sety(y)

def paddle_a_down():
    if paddle_a.ycor() > -240:
        y = paddle_a.ycor()
        y -= 20
        paddle_a.sety(y)

def paddle_b_down():
    if paddle_b.ycor() > -240:
        y = paddle_b.ycor()
        y -= 20
        paddle_b.sety(y)

# keybord binding
wn.listen()
wn.onkeypress(paddle_a_up, "z")
wn.onkeypress(paddle_b_up, "Up")
wn.onkeypress(paddle_a_down, "s")
wn.onkeypress(paddle_b_down, "Down")

# Main
while not gameOver:
    wn.update()

    # move ball
    ball.setx(ball.xcor()+ ball.dx)
    ball.sety(ball.ycor()+ ball.dy)

    # border checking
    if ball.ycor() > 290:
        ball.sety(290)
        ball.dy *= -1

    if ball.ycor() < -290:
        ball.sety(-290)
        ball.dy *= -1

    if ball.xcor() < -390:
        ball.goto(0, 0)
        score_b += 1
        if score_b == 5:
            gameOver = True
            penGagnantBleu.write("Gagnant Bleu", align="center", font=("Courier", 24, "normal"))
        else:
            ball.dx *= -1
        pen.clear()
        pen.write("Joueur Bleu: {}  Joueur Rouge: {}".format(score_a, score_b), align="center", font=("Courier", 24, "normal"))


    if ball.xcor() > 390:
        ball.goto(0, 0)
        score_a += 1
        if score_a == 5:
            gameOver = True
            penGagnantRed.write("Gagnant Rouge", align="center", font=("Courier", 24, "normal"))
        else:
            ball.dx *= -1
        pen.clear()
        pen.write("Joueur Bleu: {}  Joueur Rouge: {}".format(score_a, score_b), align="center", font=("Courier", 24, "normal"))


    #paddle collision
    if (ball.xcor() > 340 and ball.xcor() < 350) and (ball.ycor() < paddle_b.ycor() + 60 and ball.ycor() > paddle_b.ycor() - 60 ):
        ball.setx(340)
        ball.dx *= -1
        ball.color("red")
    
    if (ball.xcor() < -340 and ball.xcor() > -350) and (ball.ycor() < paddle_a.ycor() + 60 and ball.ycor() > paddle_a.ycor() - 60 ):
        ball.setx(-340)
        ball.dx *= -1
        ball.color("blue")

    if gameOver:
        time.sleep(3)