package bibliotheque;

import java.awt.Desktop;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kevch
 */
public class JFrameBibliotheque extends javax.swing.JFrame {

    /**
     * Creates new form JFrameBibliotheque
     */
    public JFrameBibliotheque() {
        initComponents();
        setResizable(false);
        setLocationRelativeTo(null);
        setTitle("Bibliotheque");
        Image icone = Toolkit.getDefaultToolkit().getImage("Image.PNG");
        this.setIconImage(icone);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel_Titre_DVD = new javax.swing.JLabel();
        jLabel_Titre_Livres = new javax.swing.JLabel();
        jComboBox_SelectDossier_DVD = new javax.swing.JComboBox();
        jComboBox_SelectDossier_Livres = new javax.swing.JComboBox();
        jButton_Ouvrir_DVD = new javax.swing.JButton();
        jButton_Ouvrir_Livres = new javax.swing.JButton();
        jTextField_EntrerDVD = new javax.swing.JTextField();
        jTextField_EntrerLivres = new javax.swing.JTextField();
        jButton_AjouterDVD = new javax.swing.JButton();
        jButton_SupprimerDVD = new javax.swing.JButton();
        jButton_AjouterLivres = new javax.swing.JButton();
        jButton_SupprimerLivres = new javax.swing.JButton();
        jComboBox_SelectDossier_Livres2 = new javax.swing.JComboBox();
        jComboBox_SelectDossier_DVD2 = new javax.swing.JComboBox();
        jTextField_EntrerDVD2 = new javax.swing.JTextField();
        jTextField_EntrerLivres2 = new javax.swing.JTextField();
        jButton_Recherche_Livres = new javax.swing.JButton();
        jButton_Recherche_DVD = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setBackground(new java.awt.Color(51, 102, 0));

        jLabel_Titre_DVD.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel_Titre_DVD.setText("DVD");
        jLabel_Titre_DVD.setMaximumSize(new java.awt.Dimension(40, 20));

        jLabel_Titre_Livres.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel_Titre_Livres.setText("LIVRES");
        jLabel_Titre_Livres.setMaximumSize(new java.awt.Dimension(40, 20));

        jComboBox_SelectDossier_DVD.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionner un dossier", "Action", "Animation", "Comédie", "Documentaire", "Drame", "Fantastique", "Horreur", "Policier", "Science Fiction", "Thriller", "Western" }));
        jComboBox_SelectDossier_DVD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_SelectDossier_DVDActionPerformed(evt);
            }
        });

        jComboBox_SelectDossier_Livres.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionner un dossier", "Action", "Arts", "Aventure", "Biographie", "Classique", "Conte", "Cuisine", "Fantastique", "Historique", "Loisir", "Mythes et Legendes", "Nature", "Nouvelle", "Paranormal", "Peinture", "Philosophie", "Policier", "Psychologie", "Récit", "Roman", "Santé", "Science Fiction", "Science", "Thriller" }));
        jComboBox_SelectDossier_Livres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_SelectDossier_LivresActionPerformed(evt);
            }
        });

        jButton_Ouvrir_DVD.setText("Ouvrir");
        jButton_Ouvrir_DVD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_Ouvrir_DVDActionPerformed(evt);
            }
        });

        jButton_Ouvrir_Livres.setText("Ouvrir");
        jButton_Ouvrir_Livres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_Ouvrir_LivresActionPerformed(evt);
            }
        });

        jTextField_EntrerDVD.setText("Entrer le nom d'un DVD");

        jTextField_EntrerLivres.setText("Entrer le nom d'un Livre");

        jButton_AjouterDVD.setText("Ajouter un DVD");
        jButton_AjouterDVD.setMaximumSize(new java.awt.Dimension(153, 23));
        jButton_AjouterDVD.setMinimumSize(new java.awt.Dimension(153, 23));
        jButton_AjouterDVD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_AjouterDVDActionPerformed(evt);
            }
        });

        jButton_SupprimerDVD.setText("Supprimer un DVD");
        jButton_SupprimerDVD.setMaximumSize(new java.awt.Dimension(153, 23));
        jButton_SupprimerDVD.setMinimumSize(new java.awt.Dimension(153, 23));
        jButton_SupprimerDVD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_SupprimerDVDActionPerformed(evt);
            }
        });

        jButton_AjouterLivres.setText("Ajouter un Livre");
        jButton_AjouterLivres.setMaximumSize(new java.awt.Dimension(133, 23));
        jButton_AjouterLivres.setMinimumSize(new java.awt.Dimension(133, 23));
        jButton_AjouterLivres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_AjouterLivresActionPerformed(evt);
            }
        });

        jButton_SupprimerLivres.setText("Supprimer un Livre");
        jButton_SupprimerLivres.setMaximumSize(new java.awt.Dimension(153, 23));
        jButton_SupprimerLivres.setMinimumSize(new java.awt.Dimension(153, 23));
        jButton_SupprimerLivres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_SupprimerLivresActionPerformed(evt);
            }
        });

        jComboBox_SelectDossier_Livres2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionner un dossier", "Action", "Arts", "Aventure", "Biographie", "Classique", "Conte", "Cuisine", "Fantastique", "Historique", "Loisir", "Mythes et Legendes", "Nature", "Nouvelle", "Paranormal", "Peinture", "Philosophie", "Policier", "Psychologie", "Récit", "Roman", "Santé", "Science Fiction", "Science", "Thriller" }));
        jComboBox_SelectDossier_Livres2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_SelectDossier_Livres2ActionPerformed(evt);
            }
        });

        jComboBox_SelectDossier_DVD2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionner un dossier", "Action", "Animation", "Comédie", "Documentaire", "Drame", "Fantastique", "Horreur", "Policier", "Science Fiction", "Thriller", "Western" }));
        jComboBox_SelectDossier_DVD2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_SelectDossier_DVD2ActionPerformed(evt);
            }
        });

        jTextField_EntrerDVD2.setText("Entrer le nom d'un DVD");

        jTextField_EntrerLivres2.setText("Entrer le nom d'un Livre");

        jButton_Recherche_Livres.setText("Rechercher");
        jButton_Recherche_Livres.setMaximumSize(new java.awt.Dimension(153, 23));
        jButton_Recherche_Livres.setMinimumSize(new java.awt.Dimension(153, 23));
        jButton_Recherche_Livres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_Recherche_LivresActionPerformed(evt);
            }
        });

        jButton_Recherche_DVD.setText("Rechercher");
        jButton_Recherche_DVD.setMaximumSize(new java.awt.Dimension(153, 23));
        jButton_Recherche_DVD.setMinimumSize(new java.awt.Dimension(153, 23));
        jButton_Recherche_DVD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_Recherche_DVDActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(194, 194, 194)
                .addComponent(jLabel_Titre_DVD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel_Titre_Livres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(186, 186, 186))
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(134, 134, 134)
                        .addComponent(jButton_Ouvrir_DVD))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(jButton_Recherche_DVD, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton_AjouterDVD, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton_SupprimerDVD, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jComboBox_SelectDossier_DVD2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField_EntrerDVD2)
                    .addComponent(jComboBox_SelectDossier_DVD, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField_EntrerDVD, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(135, 135, 135)
                        .addComponent(jButton_Ouvrir_Livres))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(105, 105, 105)
                        .addComponent(jButton_Recherche_Livres, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(95, 95, 95))
                    .addComponent(jComboBox_SelectDossier_Livres2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox_SelectDossier_Livres, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField_EntrerLivres, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton_AjouterLivres, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50)
                        .addComponent(jButton_SupprimerLivres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jTextField_EntrerLivres2, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_Titre_Livres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_Titre_DVD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBox_SelectDossier_Livres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton_Ouvrir_Livres)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField_EntrerLivres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton_AjouterLivres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton_SupprimerLivres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(44, 44, 44)
                        .addComponent(jComboBox_SelectDossier_Livres2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField_EntrerLivres2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton_Recherche_Livres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBox_SelectDossier_DVD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton_Ouvrir_DVD)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField_EntrerDVD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton_AjouterDVD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton_SupprimerDVD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(44, 44, 44)
                        .addComponent(jComboBox_SelectDossier_DVD2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField_EntrerDVD2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton_Recherche_DVD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(42, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox_SelectDossier_DVDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_SelectDossier_DVDActionPerformed
         if (jComboBox_SelectDossier_DVD.getSelectedItem().equals("Selectionner un dossier")) {

                    JOptionPane.showMessageDialog(null, "Cette option n'éfféctue rien", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jComboBox_SelectDossier_DVDActionPerformed

    private void jComboBox_SelectDossier_LivresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_SelectDossier_LivresActionPerformed
        if (jComboBox_SelectDossier_Livres.getSelectedItem().equals("Selectionner un dossier")) {

                    JOptionPane.showMessageDialog(null, "Cette option n'éfféctue rien", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jComboBox_SelectDossier_LivresActionPerformed

    private void jComboBox_SelectDossier_Livres2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_SelectDossier_Livres2ActionPerformed
        if (jComboBox_SelectDossier_Livres.getSelectedItem().equals("Selectionner un dossier")) {

                    JOptionPane.showMessageDialog(null, "Cette option n'éfféctue rien", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_jComboBox_SelectDossier_Livres2ActionPerformed

    private void jComboBox_SelectDossier_DVD2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_SelectDossier_DVD2ActionPerformed
        if (jComboBox_SelectDossier_DVD.getSelectedItem().equals("Selectionner un dossier")) {

                    JOptionPane.showMessageDialog(null, "Cette option n'éfféctue rien", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_jComboBox_SelectDossier_DVD2ActionPerformed

    private void jButton_AjouterDVDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_AjouterDVDActionPerformed
        String combo = jComboBox_SelectDossier_DVD.getSelectedItem().toString();
        String dirName = "..\\dossier DVD\\"+ combo +"\\" + jTextField_EntrerDVD.getText();
        File destinationFile = new File(dirName);
        try {
            destinationFile.createNewFile();
            JOptionPane.showMessageDialog(null, "DVD ajouté", "Information", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(JFrameBibliotheque.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "DVD non créé (peut etre est il deja présent)", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton_AjouterDVDActionPerformed

    private void jButton_AjouterLivresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_AjouterLivresActionPerformed
        String combo = jComboBox_SelectDossier_Livres.getSelectedItem().toString();
        String dirName = "..\\dossier Livres\\"+ combo +"\\" + jTextField_EntrerLivres.getText();
        File destinationFile = new File(dirName);
        try {
            destinationFile.createNewFile();
            JOptionPane.showMessageDialog(null, "Livre ajouté", "Information", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(JFrameBibliotheque.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Livre non créé (peut etre est il deja présent)", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton_AjouterLivresActionPerformed

    private void jButton_SupprimerDVDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_SupprimerDVDActionPerformed
        String combo = jComboBox_SelectDossier_DVD.getSelectedItem().toString();
        String dirName = "..\\dossier DVD\\"+ combo +"\\" + jTextField_EntrerDVD.getText();
        File destinationFile = new File(dirName);
        if(destinationFile.isFile()) {
            destinationFile.delete();
            JOptionPane.showMessageDialog(null, "DVD supprimé", "Information", JOptionPane.INFORMATION_MESSAGE);
        } else if(!destinationFile.isFile()){
            JOptionPane.showMessageDialog(null, "DVD non supprimé (peut etre non existant)", "Error", JOptionPane.ERROR);
        }
    }//GEN-LAST:event_jButton_SupprimerDVDActionPerformed

    private void jButton_SupprimerLivresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_SupprimerLivresActionPerformed
       String combo = jComboBox_SelectDossier_Livres.getSelectedItem().toString();
        String dirName = "..\\dossier Livres\\"+ combo +"\\" + jTextField_EntrerLivres.getText();
        File destinationFile = new File(dirName);
        if(destinationFile.isFile()) {
            destinationFile.delete();
            JOptionPane.showMessageDialog(null, "Livre supprimé", "Information", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Livre non supprimé (peut etre non existant)", "Error", JOptionPane.ERROR);
        }
    }//GEN-LAST:event_jButton_SupprimerLivresActionPerformed

    private void jButton_Recherche_DVDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_Recherche_DVDActionPerformed
        String combo = jComboBox_SelectDossier_DVD.getSelectedItem().toString();
        String dirName = "../dossier DVD/"+ combo +"/" + jTextField_EntrerDVD2.getText() + "/";
        File destinationFile = new File(dirName);
        if(destinationFile.exists()) {
            JOptionPane.showMessageDialog(null, "Ce DVD fait parti du dossier", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
        else if(!destinationFile.exists()){
            JOptionPane.showMessageDialog(null, "Ce DVD n'est pas présent dans ce dossier", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_jButton_Recherche_DVDActionPerformed

    private void jButton_Recherche_LivresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_Recherche_LivresActionPerformed
        String combo = jComboBox_SelectDossier_Livres.getSelectedItem().toString();
        String dirName = "../dossier Livres/"+ combo +"/" + jTextField_EntrerLivres2.getText() + "/";
        File destinationFile = new File(dirName);
        if(destinationFile.exists()) {
            JOptionPane.showMessageDialog(null, "Ce Livre fait parti du dossier", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
        else if(!destinationFile.exists()){
            JOptionPane.showMessageDialog(null, "Ce Livre n'est pas présent dans ce dossier", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_jButton_Recherche_LivresActionPerformed

    private void jButton_Ouvrir_DVDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_Ouvrir_DVDActionPerformed
            String dirName;
            dirName = "../dossier DVD/"+ jComboBox_SelectDossier_DVD.getSelectedItem().toString()+"/";
            try {
                Desktop.getDesktop().open((new File(dirName)));
            } catch (IOException ex) {
                Logger.getLogger(JFrameBibliotheque.class.getName()).log(Level.SEVERE, null, ex);
            }
    }//GEN-LAST:event_jButton_Ouvrir_DVDActionPerformed

    private void jButton_Ouvrir_LivresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_Ouvrir_LivresActionPerformed
            String dirName;
            dirName = "../dossier Livres/"+ jComboBox_SelectDossier_Livres.getSelectedItem().toString()+"/";
            try {
                Desktop.getDesktop().open((new File(dirName)));
            } catch (IOException ex) {
                Logger.getLogger(JFrameBibliotheque.class.getName()).log(Level.SEVERE, null, ex);
            }
    }//GEN-LAST:event_jButton_Ouvrir_LivresActionPerformed
  
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameBibliotheque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameBibliotheque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameBibliotheque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameBibliotheque.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameBibliotheque().setVisible(true);
                jpanel.paintComponent(null);
            }
        });
    }
    private static bibliotheque.JPanelBibliotheque jpanel = new bibliotheque.JPanelBibliotheque();
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_AjouterDVD;
    private javax.swing.JButton jButton_AjouterLivres;
    private javax.swing.JButton jButton_Ouvrir_DVD;
    private javax.swing.JButton jButton_Ouvrir_Livres;
    private javax.swing.JButton jButton_Recherche_DVD;
    private javax.swing.JButton jButton_Recherche_Livres;
    private javax.swing.JButton jButton_SupprimerDVD;
    private javax.swing.JButton jButton_SupprimerLivres;
    private javax.swing.JComboBox jComboBox_SelectDossier_DVD;
    private javax.swing.JComboBox jComboBox_SelectDossier_DVD2;
    private javax.swing.JComboBox jComboBox_SelectDossier_Livres;
    private javax.swing.JComboBox jComboBox_SelectDossier_Livres2;
    private javax.swing.JLabel jLabel_Titre_DVD;
    private javax.swing.JLabel jLabel_Titre_Livres;
    private javax.swing.JTextField jTextField_EntrerDVD;
    private javax.swing.JTextField jTextField_EntrerDVD2;
    private javax.swing.JTextField jTextField_EntrerLivres;
    private javax.swing.JTextField jTextField_EntrerLivres2;
    // End of variables declaration//GEN-END:variables
}
