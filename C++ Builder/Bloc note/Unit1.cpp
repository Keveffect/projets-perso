//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SaveDialog(TObject *Sender)
{
	 if (SaveDialog1->Execute() == true) {
		 Memo1->Lines->SaveToFile(SaveDialog1->FileName);
	 }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::OpenFile(TObject *Sender)
{
	 if (OpenDialog1->Execute() == true) {
		 Memo1->Lines->LoadFromFile(OpenDialog1->FileName);
	 }
}
//---------------------------------------------------------------------------
