object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Bloc note'
  ClientHeight = 305
  ClientWidth = 626
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  DesignSize = (
    626
    305)
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 624
    Height = 303
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    ExplicitWidth = 633
    ExplicitHeight = 297
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.txt'
    Filter = 'Text files (*.txt)|txt'
    Left = 280
    Top = 96
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '.txt'
    Left = 360
    Top = 112
  end
  object MainMenu1: TMainMenu
    Left = 136
    Top = 168
    object File1: TMenuItem
      Caption = 'File'
      object SaveAs1: TMenuItem
        Caption = 'Save As'
        OnClick = SaveDialog
      end
      object Open1: TMenuItem
        Caption = 'Open'
        OnClick = OpenFile
      end
    end
  end
end
