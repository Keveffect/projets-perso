//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "CustomPeriod.h"

#include <string>
#include <ctime>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
    //DateTimePicker1->DateTime = std::localtime(std::time(0));
}
//---------------------------------------------------------------------------


void __fastcall TForm1::ButtonExempleClick(TObject *Sender)
{
	EditFullName->Text = "CHARLIER K�vin";
	EditEMail->Text = "kevin.charlier@sparflex.com";
	EditAge->Text = "21";
	ComboBoxGender->ItemIndex = 0;
	ListBoxMagazine->ItemIndex = rand()%5;
	RadioButton3->Checked = true;
	//RadioButton4->Caption = std::to_string(rand()%36) + " month(s)";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonCustomClick(TObject *Sender)
{
	//CleanRadioButon(Sender);
	RadioButton4->Checked = true;
	//String input = InputBox("Subcription period", "Input number of moths: ", "5");
	//RadioButton4->Caption = input + " month(s)";
	Form2->ShowModal();
	if(Form2->ShowModal() == mrOk){
		if(Form2->Edit1->Text == "" || Form2->Edit1->Text == "0" || Form2->Edit1->Text > "36")
			RadioButton4->Caption = "36 month(s)";
		else
			RadioButton4->Caption = Form2->Edit1->Text + " month(s)";
	}
	else{
		RadioButton4->Checked = false;
        RadioButton4->Caption = "custom";
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CleanRadioButon(TObject *Sender){
	 //RadioButton1->checked = false;
	 RadioButton2->Checked = false;
	 RadioButton3->Checked = false;
	 RadioButton4->Checked = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonSubscribeClick(TObject *Sender)
{
	bool boolOk = true;

	if(CheckBoxTerms->Checked == false){
		ShowMessage("You must agree terms");
		boolOk = false;
	}
	else if(EditFullName->Text == "") {
		ShowMessage("Missing Name");
		boolOk = false;
	}
	else if(EditAge->Text == "") {
		ShowMessage("Missing Age");
		boolOk = false;
	}
	else if(EditAge->Text < "12"){
		ShowMessage("You ne to be at least 12yo");
		boolOk = false;
    }

	if(boolOk){
		  Memo1->Lines->Clear();

		  Memo1->Lines->Add("full name: " + EditFullName->Text);
		  Memo1->Lines->Add("e-mail: " + EditEMail->Text);
		  Memo1->Lines->Add("age: " + EditAge->Text);
		  Memo1->Lines->Add("gender: " + ComboBoxGender->Text);
		  Memo1->Lines->Add("date: " + DateTimePicker1->DateTime);
		  Memo1->Lines->Add("Magazines selected: ");
		  for(int i = 0; i < ListBoxMagazine->Count; i++)
			if(ListBoxMagazine->Selected[i] == true)
			   Memo1->Lines->Add("	" + ListBoxMagazine->Items->Strings[i]);

		  if(RadioButton1->Checked)
			Memo1->Lines->Add("Subscription period: " + RadioButton1->Caption);
		  else if(RadioButton2->Checked)
			Memo1->Lines->Add("Subscription period: " + RadioButton2->Caption);
		  else if(RadioButton3->Checked)
			Memo1->Lines->Add("Subscription period: " + RadioButton3->Caption);
		  else if(RadioButton4->Checked)
			Memo1->Lines->Add("Subscription period: " + RadioButton4->Caption);
	}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonSaveAsClick(TObject *Sender)
{
	if(SaveDialog1->Execute() == true){
		Memo1->Lines->SaveToFile(SaveDialog1->FileName);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonOpenClick(TObject *Sender)
{
	if(OpenDialog1->Execute() == true){
		Memo1->Lines->LoadFromFile(SaveDialog1->FileName);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Exit2Click(TObject *Sender)
{
    Application->Terminate();
}
//---------------------------------------------------------------------------



void __fastcall TForm1::ButtonClearClick(TObject *Sender)
{
    Memo1->Lines->Clear();
}
//---------------------------------------------------------------------------

