object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Magazine Subscription'
  ClientHeight = 583
  ClientWidth = 870
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  DesignSize = (
    870
    583)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 297
    Height = 201
    Caption = 'Subscriber info'
    TabOrder = 0
    object Label1: TLabel
      Left = 22
      Top = 24
      Width = 58
      Height = 13
      Caption = 'Full name* :'
    end
    object Label3: TLabel
      Left = 22
      Top = 56
      Width = 41
      Height = 13
      Caption = 'e-mail* :'
    end
    object Label4: TLabel
      Left = 22
      Top = 88
      Width = 31
      Height = 13
      Caption = 'age* :'
    end
    object Label2: TLabel
      Left = 22
      Top = 120
      Width = 48
      Height = 13
      Caption = 'Gender* :'
    end
    object Label5: TLabel
      Left = 22
      Top = 152
      Width = 66
      Height = 13
      Caption = 'Subs. start* :'
    end
    object EditFullName: TEdit
      Left = 88
      Top = 21
      Width = 177
      Height = 21
      TabOrder = 0
    end
    object EditAge: TEdit
      Left = 88
      Top = 85
      Width = 25
      Height = 21
      NumbersOnly = True
      TabOrder = 1
      Text = '12'
    end
    object EditEMail: TEdit
      Left = 88
      Top = 53
      Width = 177
      Height = 21
      TabOrder = 2
    end
    object UpDown1: TUpDown
      Left = 113
      Top = 85
      Width = 16
      Height = 21
      Associate = EditAge
      Position = 12
      TabOrder = 3
    end
    object ComboBoxGender: TComboBox
      Left = 88
      Top = 117
      Width = 177
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 4
      Text = 'Male'
      Items.Strings = (
        'Male'
        'Female')
    end
    object DateTimePicker1: TDateTimePicker
      Left = 88
      Top = 152
      Width = 177
      Height = 21
      Date = 44666.693449328700000000
      Time = 44666.693449328700000000
      DoubleBuffered = False
      ParentDoubleBuffered = False
      TabOrder = 5
    end
  end
  object ButtonExemple: TButton
    Left = 320
    Top = 550
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Exemple'
    TabOrder = 1
    OnClick = ButtonExempleClick
  end
  object Magazine: TGroupBox
    Left = 8
    Top = 215
    Width = 297
    Height = 146
    Caption = 'Magazine*'
    TabOrder = 2
    object ListBoxMagazine: TListBox
      Left = 16
      Top = 24
      Width = 265
      Height = 73
      ItemHeight = 13
      Items.Strings = (
        'Magazine 1'
        'Magazine 2'
        'Magazine 3'
        'Magazine 4'
        'Magazine 5')
      MultiSelect = True
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 367
    Width = 297
    Height = 145
    Caption = 'Subscription period*'
    TabOrder = 3
    object RadioButton1: TRadioButton
      Left = 16
      Top = 32
      Width = 113
      Height = 17
      Caption = '1 month'
      TabOrder = 0
    end
    object RadioButton2: TRadioButton
      Left = 16
      Top = 55
      Width = 113
      Height = 17
      Caption = '6 months'
      TabOrder = 1
    end
    object RadioButton3: TRadioButton
      Left = 16
      Top = 78
      Width = 113
      Height = 17
      Caption = '12 months'
      TabOrder = 2
    end
    object RadioButton4: TRadioButton
      Left = 16
      Top = 101
      Width = 113
      Height = 17
      Caption = 'custom'
      TabOrder = 3
    end
    object ButtonCustom: TButton
      Left = 97
      Top = 101
      Width = 32
      Height = 20
      Caption = '...'
      TabOrder = 4
      OnClick = ButtonCustomClick
    end
  end
  object CheckBoxTerms: TCheckBox
    Left = 8
    Top = 520
    Width = 297
    Height = 17
    Caption = 'I agree with terms & conditions'
    TabOrder = 4
  end
  object ButtonSubscribe: TButton
    Left = 8
    Top = 549
    Width = 297
    Height = 25
    Caption = 'Subscribe'
    TabOrder = 5
    OnClick = ButtonSubscribeClick
  end
  object Memo1: TMemo
    Left = 311
    Top = 8
    Width = 551
    Height = 507
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 6
  end
  object ButtonSaveAs: TButton
    Left = 401
    Top = 549
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Save As...'
    TabOrder = 7
    OnClick = ButtonSaveAsClick
  end
  object ButtonOpen: TButton
    Left = 482
    Top = 550
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Open'
    TabOrder = 8
    OnClick = ButtonOpenClick
  end
  object ButtonClear: TButton
    Left = 563
    Top = 550
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Clear'
    TabOrder = 9
    OnClick = ButtonClearClick
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.txt'
    Filter = 'Text files (.txt)'
    Left = 496
    Top = 256
  end
  object OpenDialog1: TOpenDialog
    Left = 600
    Top = 264
  end
  object MainMenu1: TMainMenu
    Left = 552
    Top = 176
    object Exit1: TMenuItem
      Caption = 'Application'
      object Open1: TMenuItem
        Caption = 'Open'
        OnClick = ButtonOpenClick
      end
      object SaveAs1: TMenuItem
        Caption = 'Save As'
        OnClick = ButtonSaveAsClick
      end
      object Exit2: TMenuItem
        Caption = 'Exit'
        OnClick = Exit2Click
      end
    end
  end
end
