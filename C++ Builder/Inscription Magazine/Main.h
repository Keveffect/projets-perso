//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.Menus.hpp>

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// Composants gérés par l'EDI
	TGroupBox *GroupBox1;
	TLabel *Label1;
	TEdit *EditFullName;
	TLabel *Label3;
	TEdit *EditEMail;
	TLabel *Label4;
	TEdit *EditAge;
	TUpDown *UpDown1;
	TButton *ButtonExemple;
	TLabel *Label2;
	TComboBox *ComboBoxGender;
	TDateTimePicker *DateTimePicker1;
	TLabel *Label5;
	TGroupBox *Magazine;
	TListBox *ListBoxMagazine;
	TGroupBox *GroupBox2;
	TRadioButton *RadioButton1;
	TRadioButton *RadioButton2;
	TRadioButton *RadioButton3;
	TRadioButton *RadioButton4;
	TButton *ButtonCustom;
	TCheckBox *CheckBoxTerms;
	TButton *ButtonSubscribe;
	TMemo *Memo1;
	TButton *ButtonSaveAs;
	TButton *ButtonOpen;
	TSaveDialog *SaveDialog1;
	TOpenDialog *OpenDialog1;
	TMainMenu *MainMenu1;
	TMenuItem *Exit1;
	TMenuItem *Exit2;
	TMenuItem *SaveAs1;
	TMenuItem *Open1;
	TButton *ButtonClear;
	void __fastcall ButtonExempleClick(TObject *Sender);
	void __fastcall ButtonCustomClick(TObject *Sender);
	void __fastcall CleanRadioButon(TObject *Sender);
	void __fastcall ButtonSubscribeClick(TObject *Sender);
	void __fastcall ButtonSaveAsClick(TObject *Sender);
	void __fastcall ButtonOpenClick(TObject *Sender);
	void __fastcall Exit2Click(TObject *Sender);
	void __fastcall ButtonClearClick(TObject *Sender);
private:	// Déclarations utilisateur
public:		// Déclarations utilisateur
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
