//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormCalculate *FormCalculate;

Double entry1, entry2, result;
String operates;
//---------------------------------------------------------------------------
__fastcall TFormCalculate::TFormCalculate(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormCalculate::ClickNumeric(TObject *Sender)
{
	TButton* btn = ((TButton*) Sender);

	if (EditResult->Text == "0") {
		EditResult->Text = btn->Caption;
	}
	else{
		EditResult->Text = EditResult->Text + btn->Caption;
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormCalculate::ClickOperator(TObject *Sender)
{
	TButton* btn = ((TButton*) Sender);

	entry1 = EditResult->Text.ToDouble();
	operates = btn->Caption;
	EditResult->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall TFormCalculate::btnEqualsClick(TObject *Sender)
{
	entry2 = EditResult->Text.ToDouble();

	if (operates == "+") {
		result = entry1 + entry2;
        EditResult->Text = result;
	}
	else if (operates == "-") {
		result = entry1 - entry2;
		EditResult->Text = result;
	}
	else if (operates == "*") {
		result = entry1 * entry2;
		EditResult->Text = result;
	}
	else if (operates == "/") {
		result = entry1 / entry2;
		EditResult->Text = result;
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormCalculate::BtnDotClick(TObject *Sender)
{
	if (!EditResult->Text.Pos(",")) {
		EditResult->Text = EditResult->Text + ",";
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormCalculate::btnBSClick(TObject *Sender)
{
	int q = EditResult->Text.Length();

	if( q == 1){
		EditResult->Text = "0";
	}
	else{
		EditResult->Text = EditResult->Text.Delete(q,1);
    }
}
//---------------------------------------------------------------------------
void __fastcall TFormCalculate::btnCEClick(TObject *Sender)
{
	EditResult->Text = "";

	String f, s;

	f = entry1;
	s = entry2;

	f="";
    s="";
}
//---------------------------------------------------------------------------
void __fastcall TFormCalculate::btnClearClick(TObject *Sender)
{
    EditResult->Text = "0";
}
//---------------------------------------------------------------------------
void __fastcall TFormCalculate::btnPmClick(TObject *Sender)
{
	Double p = (EditResult->Text.ToDouble());
    EditResult->Text = (-1*p);
}
//---------------------------------------------------------------------------
