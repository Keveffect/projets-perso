object FormCalculate: TFormCalculate
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Calculatrice'
  ClientHeight = 561
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object EditResult: TEdit
    Left = 8
    Top = 8
    Width = 378
    Height = 66
    Alignment = taRightJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = '0'
  end
  object ButtonDigit1: TButton
    Left = 8
    Top = 370
    Width = 90
    Height = 90
    Caption = '1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = ClickNumeric
  end
  object ButtonDigit2: TButton
    Left = 104
    Top = 370
    Width = 90
    Height = 90
    Caption = '2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = ClickNumeric
  end
  object ButtonDigit3: TButton
    Left = 200
    Top = 370
    Width = 90
    Height = 90
    Caption = '3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = ClickNumeric
  end
  object ButtonMultiplie: TButton
    Left = 296
    Top = 370
    Width = 90
    Height = 90
    Caption = '*'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = ClickOperator
  end
  object ButtonDigit4: TButton
    Left = 8
    Top = 274
    Width = 90
    Height = 90
    Caption = '4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = ClickNumeric
  end
  object ButtonDigit5: TButton
    Left = 104
    Top = 274
    Width = 90
    Height = 90
    Caption = '5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = ClickNumeric
  end
  object ButtonDigit6: TButton
    Left = 200
    Top = 274
    Width = 90
    Height = 90
    Caption = '6'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = ClickNumeric
  end
  object BtnSub: TButton
    Left = 296
    Top = 274
    Width = 90
    Height = 90
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = ClickOperator
  end
  object ButtonDigit7: TButton
    Left = 8
    Top = 178
    Width = 90
    Height = 90
    Caption = '7'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = ClickNumeric
  end
  object ButtonDigit8: TButton
    Left = 104
    Top = 178
    Width = 90
    Height = 90
    Caption = '8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    OnClick = ClickNumeric
  end
  object ButtonDigit9: TButton
    Left = 200
    Top = 178
    Width = 90
    Height = 90
    Caption = '9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    OnClick = ClickNumeric
  end
  object BtnAdd: TButton
    Left = 296
    Top = 178
    Width = 90
    Height = 90
    Caption = '+'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    OnClick = ClickOperator
  end
  object btnBS: TButton
    Left = 8
    Top = 79
    Width = 90
    Height = 90
    Caption = #61653
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Wingdings'
    Font.Style = []
    ParentFont = False
    TabOrder = 13
    OnClick = btnBSClick
  end
  object btnCE: TButton
    Left = 104
    Top = 79
    Width = 90
    Height = 90
    Caption = 'CE'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 14
    OnClick = btnCEClick
  end
  object btnClear: TButton
    Left = 200
    Top = 79
    Width = 90
    Height = 90
    Caption = 'C'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 15
    OnClick = btnClearClick
  end
  object btnPm: TButton
    Left = 296
    Top = 79
    Width = 90
    Height = 90
    Caption = #177
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 16
    OnClick = btnPmClick
  end
  object ButtonDigit0: TButton
    Left = 8
    Top = 466
    Width = 90
    Height = 90
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 17
    OnClick = ClickNumeric
  end
  object BtnDot: TButton
    Left = 104
    Top = 466
    Width = 90
    Height = 90
    Caption = ','
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 18
    OnClick = BtnDotClick
  end
  object BtnEquals: TButton
    Left = 200
    Top = 466
    Width = 90
    Height = 90
    Caption = '='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 19
    OnClick = btnEqualsClick
  end
  object BtnDivide: TButton
    Left = 296
    Top = 466
    Width = 90
    Height = 90
    Caption = '/'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -48
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 20
    OnClick = ClickOperator
  end
end
