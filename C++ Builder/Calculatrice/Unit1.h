//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TFormCalculate : public TForm
{
__published:	// Composants gérés par l'EDI
	TEdit *EditResult;
	TButton *ButtonDigit1;
	TButton *ButtonDigit2;
	TButton *ButtonDigit3;
	TButton *ButtonMultiplie;
	TButton *ButtonDigit4;
	TButton *ButtonDigit5;
	TButton *ButtonDigit6;
	TButton *BtnSub;
	TButton *ButtonDigit7;
	TButton *ButtonDigit8;
	TButton *ButtonDigit9;
	TButton *BtnAdd;
	TButton *btnBS;
	TButton *btnCE;
	TButton *btnClear;
	TButton *btnPm;
	TButton *ButtonDigit0;
	TButton *BtnDot;
	TButton *BtnEquals;
	TButton *BtnDivide;
	void __fastcall ClickNumeric(TObject *Sender);
	void __fastcall ClickOperator(TObject *Sender);
	void __fastcall btnEqualsClick(TObject *Sender);
	void __fastcall BtnDotClick(TObject *Sender);
	void __fastcall btnBSClick(TObject *Sender);
	void __fastcall btnCEClick(TObject *Sender);
	void __fastcall btnClearClick(TObject *Sender);
	void __fastcall btnPmClick(TObject *Sender);
private:	// Déclarations utilisateur
public:		// Déclarations utilisateur
	__fastcall TFormCalculate(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormCalculate *FormCalculate;
//---------------------------------------------------------------------------
#endif
